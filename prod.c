#include <stdio.h>
#include <sys/time.h>
#include <stdlib.h>

int s = 0;
static int N = 1024;
int rand()
{
    return (3218761 * (s++) + 1231231) % 100007;
}
int idx(int i, int j)
{
    return N * i + j;
}
void fill_rand_arr(int *x)
{
    for (int i = 0; i < 5; i++)
        for (int j = 0; j < 5; j++)
            // x[i][j] = rand();
            x[idx(i, j)] = 0;
}

void prod(int *a, int *b, int *c)
{
    // fills c = a*b
    for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++)
        {
            c[idx(i, j)] = 0;
            for (int k = 0; k < N; k++)
                c[idx(i, j)] += a[idx(i, k)] * b[idx(k, j)];
        }
}

void prod_tr(int *a, int *b_tr, int *c)
{
    // fills c = a*b, given b transpose b_tr
    for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++)
        {
            c[idx(i, j)] = 0;
            for (int k = 0; k < N; k++)
                c[idx(i, j)] += a[idx(i, k)] * b_tr[idx(j, k)];
        }
}

double time_s(struct timeval *tv)
{
    return (double)tv->tv_sec + ((double)tv->tv_usec) / 1000000.0;
}

int main(int argc, char **argv)
{
    int type = 0;
    if (argc > 1)
        type = argv[1][0] - '0';

    // int a[N][N], b[N][N], c[N][N];
    int *a = malloc(N * N * sizeof(int));
    int *b = malloc(N * N * sizeof(int));
    int *c = malloc(N * N * sizeof(int));
    struct timeval tv;

    gettimeofday(&tv, NULL);
    double prefill_ms = time_s(&tv);

    fill_rand_arr(a);
    fill_rand_arr(b);

    gettimeofday(&tv, NULL);
    double start_ms = time_s(&tv);
    printf("time to fill arr: %f\n", (-prefill_ms + start_ms));

    if (type)
        prod(a, b, c);
    else
        prod_tr(a, b, c);

    gettimeofday(&tv, NULL);
    printf("%ld %d\n", tv.tv_sec, tv.tv_usec);
    double end_ms = time_s(&tv);

    printf("type: %s\n", type ? "row, row" : "row, col");
    printf("time: %f s\n", (end_ms - start_ms));
}